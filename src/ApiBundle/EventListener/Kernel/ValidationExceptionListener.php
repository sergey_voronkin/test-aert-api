<?php

namespace ApiBundle\EventListener\Kernel;

use ApiBundle\Exception\ValidationException;
use ApiBundle\Response\ErrorResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ValidationExceptionListener
 *
 * @package AppBundle\EventListener\Kernel
 */
class ValidationExceptionListener
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof ValidationException) {

            $error = $exception->getErrors()->getIterator()->current();
            $payload = $error->getConstraint()->payload ?? null;

            $errorCode = $payload['error_code'] ?? '1.01';

            $errorMessage = $this->translator->trans($errorCode, [], 'api_errors');

            $response = new ErrorResponse($errorCode, $errorMessage);

            $event->setResponse($response);

            $event->stopPropagation();
        }
    }
}
