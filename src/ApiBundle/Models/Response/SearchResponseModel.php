<?php

namespace ApiBundle\Models\Response;

use ApiBundle\Models\Common\FlightModel;
use ApiBundle\Models\Common\SearchQueryModel;

/**
 * Class SearchResponseModel
 *
 * @package ApiBundle\Models\Response
 */
class SearchResponseModel
{
    /**
     * @var SearchQueryModel
     */
    private $searchQuery;

    /**
     * @var FlightModel[]
     */
    private $searchResults;

    /**
     * @return SearchQueryModel
     */
    public function getSearchQuery(): SearchQueryModel
    {
        return $this->searchQuery;
    }

    /**
     * @param SearchQueryModel $searchQuery
     *
     * @return SearchResponseModel
     */
    public function setSearchQuery(SearchQueryModel $searchQuery): SearchResponseModel
    {
        $this->searchQuery = $searchQuery;

        return $this;
    }

    /**
     * @return FlightModel[]
     */
    public function getSearchResults(): array
    {
        return $this->searchResults;
    }

    /**
     * @param FlightModel[] $searchResults
     *
     * @return SearchResponseModel
     */
    public function setSearchResults(array $searchResults): SearchResponseModel
    {
        $this->searchResults = $searchResults;

        return $this;
    }

}