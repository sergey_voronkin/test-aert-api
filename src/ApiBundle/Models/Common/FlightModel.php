<?php

namespace ApiBundle\Models\Common;

/**
 * Class FlightModel
 *
 * @package ApiBundle\Models\Common
 */
class FlightModel
{
    /**
     * @var TransporterModel
     */
    private $transporter;

    /**
     * @var string
     */
    private $flightNumber;

    /**
     * @var string
     */
    private $departureAirport;

    /**
     * @var string
     */
    private $arrivalAirport;

    /**
     * @var string
     */
    private $departureDateTime;

    /**
     * @var string
     */
    private $arrivalDateTime;

    /**
     * @var string
     */
    private $duration;

    /**
     * @return TransporterModel
     */
    public function getTransporter(): TransporterModel
    {
        return $this->transporter;
    }

    /**
     * @param TransporterModel $transporter
     *
     * @return FlightModel
     */
    public function setTransporter(TransporterModel $transporter): FlightModel
    {
        $this->transporter = $transporter;

        return $this;
    }

    /**
     * @return string
     */
    public function getFlightNumber(): string
    {
        return $this->flightNumber;
    }

    /**
     * @param string $flightNumber
     *
     * @return FlightModel
     */
    public function setFlightNumber(string $flightNumber): FlightModel
    {
        $this->flightNumber = $flightNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getDepartureAirport(): string
    {
        return $this->departureAirport;
    }

    /**
     * @param string $departureAirport
     *
     * @return FlightModel
     */
    public function setDepartureAirport(string $departureAirport): FlightModel
    {
        $this->departureAirport = $departureAirport;

        return $this;
    }

    /**
     * @return string
     */
    public function getArrivalAirport(): string
    {
        return $this->arrivalAirport;
    }

    /**
     * @param string $arrivalAirport
     *
     * @return FlightModel
     */
    public function setArrivalAirport(string $arrivalAirport): FlightModel
    {
        $this->arrivalAirport = $arrivalAirport;

        return $this;
    }

    /**
     * @return string
     */
    public function getDepartureDateTime(): string
    {
        return $this->departureDateTime;
    }

    /**
     * @param string $departureDateTime
     *
     * @return FlightModel
     */
    public function setDepartureDateTime(string $departureDateTime): FlightModel
    {
        $this->departureDateTime = $departureDateTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getArrivalDateTime(): string
    {
        return $this->arrivalDateTime;
    }

    /**
     * @param string $arrivalDateTime
     *
     * @return FlightModel
     */
    public function setArrivalDateTime(string $arrivalDateTime): FlightModel
    {
        $this->arrivalDateTime = $arrivalDateTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     *
     * @return FlightModel
     */
    public function setDuration(string $duration): FlightModel
    {
        $this->duration = $duration;

        return $this;
    }
}