<?php

namespace ApiBundle\Models\Common;

/**
 * Class TransporterModel
 *
 * @package ApiBundle\Models\Common
 */
class TransporterModel
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return TransporterModel
     */
    public function setCode(string $code): TransporterModel
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TransporterModel
     */
    public function setName(string $name): TransporterModel
    {
        $this->name = $name;

        return $this;
    }
}