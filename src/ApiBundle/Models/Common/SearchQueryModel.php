<?php

namespace ApiBundle\Models\Common;

use AppBundle\Contract\SearchQueryInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SearchQueryModel
 *
 * @package ApiBundle\Models\Common
 */
class SearchQueryModel implements SearchQueryInterface
{
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(min = 3, max = 3, payload={"error_code" = "1.02"})
     */
    private $departureAirport;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(min = 3, max = 3, payload={"error_code" = "1.02"})
     */
    private $arrivalAirport;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Date(payload={"error_code" = "1.03"})
     */
    private $departureDate;

    /**
     * @return string
     */
    public function getDepartureDate(): string
    {
        return $this->departureDate;
    }

    /**
     * @param string $departureDate
     */
    public function setDepartureDate(string $departureDate): void
    {
        $this->departureDate = $departureDate;
    }

    /**
     * @return string
     */
    public function getDepartureAirport(): string
    {
        return $this->departureAirport;
    }

    /**
     * @param string $departureAirport
     */
    public function setDepartureAirport(string $departureAirport): void
    {
        $this->departureAirport = $departureAirport;
    }

    /**
     * @return string
     */
    public function getArrivalAirport(): string
    {
        return $this->arrivalAirport;
    }

    /**
     * @param string $arrivalAirport
     */
    public function setArrivalAirport(string $arrivalAirport): void
    {
        $this->arrivalAirport = $arrivalAirport;
    }

}