<?php

namespace ApiBundle\Models\Request;

use ApiBundle\Models\Common\SearchQueryModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SearchRequestModel
 *
 * @package ApiBundle\Models\Request
 */
class SearchRequestModel
{
    /**
     * @var SearchQueryModel
     * @Assert\NotBlank
     * @Assert\Valid()
     */
    private $searchQuery;

    /**
     * @return SearchQueryModel
     */
    public function getSearchQuery(): SearchQueryModel
    {
        return $this->searchQuery;
    }

    /**
     * @param SearchQueryModel $searchQuery
     *
     * @return SearchRequestModel
     */
    public function setSearchQuery(SearchQueryModel $searchQuery): SearchRequestModel
    {
        $this->searchQuery = $searchQuery;

        return $this;
    }
}