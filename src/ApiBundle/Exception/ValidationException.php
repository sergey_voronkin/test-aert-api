<?php

namespace ApiBundle\Exception;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class ValidationException
 *
 * @package ApiBundle\Exception
 */
class ValidationException extends \Exception
{
    /**
     * @var string
     */
    protected $errorCode = '0.01';

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @var ConstraintViolationList
     */
    private $errors;
    /**
     * ValidationException constructor.
     *
     * @param ConstraintViolationList $violations
     */
    public function __construct(ConstraintViolationList $violations)
    {
        $this->errors = $violations;

        /** @var ConstraintViolation $iterator */
        $iterator = $violations->getIterator()->current();

        parent::__construct('Validation error: ' . $iterator->getPropertyPath(). ' - '.$iterator->getMessage() );
    }

    /**
     * @return ConstraintViolationList
     */
    public function getErrors(): ConstraintViolationList
    {
        return $this->errors;
    }

}