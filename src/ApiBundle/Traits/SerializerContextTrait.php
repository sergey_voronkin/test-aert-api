<?php

namespace ApiBundle\Traits;

use ApiBundle\Serializer\JsonSerializer;
use Symfony\Component\Serializer\Serializer;

/**
 * Trait SerializerContextTrait
 *
 * @package ApiBundle\Traits
 */
trait SerializerContextTrait
{
    /**
     * @var JsonSerializer
     */
    protected $serializer;

    /**
     * @return JsonSerializer|Serializer
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function getSerializer()
    {
        if (!($this->serializer instanceof Serializer)) {
            $this->serializer = new JsonSerializer();
        }

        return $this->serializer;
    }

    /**
     * @param Serializer $serializer
     */
    public function setSerializer(Serializer $serializer): void
    {
        $this->serializer = $serializer;
    }
}