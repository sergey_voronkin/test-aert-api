<?php

namespace ApiBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class FlightsNotFoundResponse
 *
 * @package ApiBundle\Response
 */
class FlightsNotFoundResponse extends JsonResponse
{
    /**
     * ErrorResponse constructor.
     *
     * @param string $errorCode
     * @param string $errorMessage
     * @param array  $headers
     */
    public function __construct(
        string $errorCode,
        string $errorMessage,
        $headers = []
    ) {
        $data = [
            'error'  => [
                'code'    => $errorCode,
                'message' => $errorMessage,
            ],
        ];

        parent::__construct($data, Response::HTTP_OK, $headers);
        $this->headers->set('X-Status-Code', Response::HTTP_OK);
    }
}