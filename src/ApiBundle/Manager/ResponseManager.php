<?php

namespace ApiBundle\Manager;

use ApiBundle\Models\Common\FlightModel;
use ApiBundle\Models\Common\SearchQueryModel;
use ApiBundle\Models\Common\TransporterModel;
use ApiBundle\Models\Response\SearchResponseModel;
use ApiBundle\Response\ErrorResponse;
use ApiBundle\Traits\SerializerContextTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ResponseManager
 *
 * @package ApiBundle\Manager
 */
class ResponseManager
{
    use SerializerContextTrait;

    private const NOT_FOUND_ERROR_CODE = '5.01';

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ResponseManager constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param SearchQueryModel $queryModel
     * @param array            $flights
     *
     * @return Response
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function createSearchResponse(SearchQueryModel $queryModel, array $flights)
    {
        if (empty($flights)) {
            return $this->createNotFoundResponse();
        }

        //TODO: для этой логики лучше реализовать отдельный механизм сериализации
        // для упрощения оставляю как есть

        $searchResponseModel = new SearchResponseModel();
        $searchResponseModel->setSearchQuery($queryModel);

        $flightModels = [];

        foreach ($flights as $flight) {
            $transporter = (new TransporterModel())
                ->setCode($flight->getTransporter()->getCode())
                ->setName($flight->getTransporter()->getName());

            $flightModel = (new FlightModel())
                ->setArrivalAirport($flight->getArrivalAirport()->getCode())
                ->setDepartureAirport($flight->getDepartureAirport()->getCode())
                ->setArrivalDateTime($flight->getArrivalDateTime()->format('Y-m-d H:i:s.u'))
                ->setDepartureDateTime($flight->getDepartureDateTime()->format('Y-m-d H:i:s.u'))
                ->setDuration($flight->getDuration())
                ->setFlightNumber($flight->getFlightNumber())
                ->setTransporter($transporter);

            $flightModels[] = $flightModel;
        }

        $searchResponseModel->setSearchResults($flightModels);

        $json = $this->getSerializer()->serialize($searchResponseModel, 'json');

        return Response::create($json);
    }

    /**
     * @return ErrorResponse
     */
    public function createNotFoundResponse()
    {
        $errorCode = self::NOT_FOUND_ERROR_CODE;

        $errorMessage = $this->translator->trans($errorCode, [], 'api_errors');

        return new ErrorResponse($errorCode, $errorMessage);
    }
}