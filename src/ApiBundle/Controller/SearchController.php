<?php

namespace ApiBundle\Controller;

use ApiBundle\Models\Request\SearchRequestModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SearchController
 *
 * @package ApiBundle\Controller
 */
class SearchController extends AbstractApiController
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \ApiBundle\Exception\ValidationException
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function searchAction(Request $request)
    {
        /** @var SearchRequestModel $searchRequestModel */
        $searchRequestModel = $this->getRequestModel($request, SearchRequestModel::class);

        $flights = $this->get('app.service.flight_search')->searchFlights($searchRequestModel->getSearchQuery());

        $response = $this->get('api.manager.response_manager')->createSearchResponse($searchRequestModel->getSearchQuery(), $flights);

        return $response;
    }
}
