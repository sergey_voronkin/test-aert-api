<?php

namespace ApiBundle\Controller;

use ApiBundle\Exception\ValidationException;
use ApiBundle\Traits\SerializerContextTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractApiController
 *
 * @package AuthBundle\Controller
 */
abstract class AbstractApiController extends Controller
{
    use SerializerContextTrait;

    /**
     * @param       $model
     * @param array $context
     *
     * @return array|bool|float|int|mixed|string
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function toArray($model, array $context = ['groups' => ['out']])
    {
        return $this->getSerializer()->normalize($model, null, $context);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getContent(Request $request)
    {
        $content = $request->getContent();

        try {
            $result = json_encode(json_decode($content, true));
        } catch (\Throwable $e) {
            $result = '{}';
        }

        return $result;
    }

    /**
     * @param Request $request
     * @param string  $modelClassName
     *
     * @return object
     * @throws ValidationException
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function getRequestModel(Request $request, string $modelClassName)
    {
        $data = $this->getContent($request);

        $requestModel = $this->getSerializer()->deserialize($data, $modelClassName, 'json');

        $errors = $this->get('validator')->validate($requestModel);

        if ($errors->count() > 0) {
            throw new ValidationException($errors);
        }

        return $requestModel;
    }

    /**
     * @return bool
     */
    public function isPremium()
    {
       return ($this->getUser()->isPremium());
    }
}