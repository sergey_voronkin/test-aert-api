<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Flight
 *
 * @ORM\Table(name="flight")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FlightRepository")
 */
class Flight
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Airport
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Airport", inversedBy="departureFlights")
     * @ORM\JoinColumn(name="departure_airport_id", referencedColumnName="id", nullable=false)
     */
    private $departureAirport;

    /**
     * @var Airport
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Airport", inversedBy="arrivalFlights")
     * @ORM\JoinColumn(name="arrival_airport_id", referencedColumnName="id", nullable=false)
     */
    private $arrivalAirport;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="departure_date_time", type="datetime")
     */
    private $departureDateTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="arrival_date_time", type="datetime")
     */
    private $arrivalDateTime;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="flight_number", type="string", length=16, unique=true)
     */
    private $flightNumber;

    /**
     * @var Transporter
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Transporter", inversedBy="flights")
     * @ORM\JoinColumn(name="transporter_id", referencedColumnName="id", nullable=false)
     */
    private $transporter;

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     *
     * @return Flight
     */
    public function setDuration(int $duration): Flight
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Flight
     */
    public function setId(int $id): Flight
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport(): Airport
    {
        return $this->departureAirport;
    }

    /**
     * @param Airport $departureAirport
     *
     * @return Flight
     */
    public function setDepartureAirport(Airport $departureAirport): Flight
    {
        $this->departureAirport = $departureAirport;

        return $this;
    }

    /**
     * @return Airport
     */
    public function getArrivalAirport(): Airport
    {
        return $this->arrivalAirport;
    }

    /**
     * @param Airport $arrivalAirport
     *
     * @return Flight
     */
    public function setArrivalAirport(Airport $arrivalAirport): Flight
    {
        $this->arrivalAirport = $arrivalAirport;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDepartureDateTime(): \DateTime
    {
        return $this->departureDateTime;
    }

    /**
     * @param \DateTime $departureDateTime
     *
     * @return Flight
     */
    public function setDepartureDateTime(\DateTime $departureDateTime): Flight
    {
        $this->departureDateTime = $departureDateTime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getArrivalDateTime(): \DateTime
    {
        return $this->arrivalDateTime;
    }

    /**
     * @param \DateTime $arrivalDateTime
     *
     * @return Flight
     */
    public function setArrivalDateTime(\DateTime $arrivalDateTime): Flight
    {
        $this->arrivalDateTime = $arrivalDateTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getFlightNumber(): string
    {
        return $this->flightNumber;
    }

    /**
     * @param string $flightNumber
     *
     * @return Flight
     */
    public function setFlightNumber(string $flightNumber): Flight
    {
        $this->flightNumber = $flightNumber;

        return $this;
    }

    /**
     * @return Transporter
     */
    public function getTransporter(): Transporter
    {
        return $this->transporter;
    }

    /**
     * @param Transporter $transporter
     *
     * @return Flight
     */
    public function setTransporter(Transporter $transporter): Flight
    {
        $this->transporter = $transporter;

        return $this;
    }

}

