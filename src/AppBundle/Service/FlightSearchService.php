<?php

namespace AppBundle\Service;

use AppBundle\Contract\SearchQueryInterface;
use AppBundle\Entity\Flight;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class FlightSearchService
 *
 * @package AppBundle\Service
 */
class FlightSearchService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * FlightSearchService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @param SearchQueryInterface $searchQuery
     *
     * @return mixed
     */
    public function searchFlights(SearchQueryInterface $searchQuery)
    {
        return $this->entityManager
            ->getRepository(Flight::class)
            ->findAllFlightsBy(
                $searchQuery->getArrivalAirport(),
                $searchQuery->getDepartureAirport(),
                $searchQuery->getDepartureDate()
            );
    }
}