<?php


namespace AppBundle\Contract;

/**
 * Interface SearchQueryInterface
 *
 * @package AppBundle\Contract
 */
interface SearchQueryInterface
{
    /**
     * @return \DateTime
     */
    public function getDepartureDate();

    /**
     * @return string
     */
    public function getDepartureAirport();

    /**
     * @return string
     */
    public function getArrivalAirport();

}