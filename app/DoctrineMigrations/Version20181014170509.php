<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181014170509 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("INSERT INTO transporter (id, name, code) VALUES 
            (1, 'WizzAir', 'W6'),
            (2, 'UkraineInternational', 'PS'),
            (3, 'SuperAir', 'SA')");

        $this->addSql("INSERT INTO airport (id, name, code, time_zone) VALUES 
            (1, 'Kyiv', 'IEV', 'Europe/Kiev'),
            (2, 'Warsaw', 'WRS', 'Europe/Warsaw'),
            (3, 'Berlin', 'BRL', 'Europe/Berlin')");

        $this->addSql("INSERT INTO flight (id, departure_airport_id, arrival_airport_id, transporter_id, departure_date_time, arrival_date_time, flight_number, duration) VALUES
            (1, 1, 2, 1, '2018-11-14 01:27:43', '2018-11-14 02:27:50', 'W64556', 12),
            (2, 1, 3, 1, '2018-11-14 02:27:43', '2018-11-14 03:27:50', 'W64557', 23),
            (3, 2, 1, 1, '2018-11-14 03:27:43', '2018-11-14 04:27:50', 'W64553', 34),
            (4, 2, 3, 1, '2018-11-14 04:27:43', '2018-11-14 05:27:50', 'W64552', 45),
            (5, 3, 1, 1, '2018-11-14 05:27:43', '2018-11-14 06:27:50', 'W74553', 56),
            (6, 3, 2, 1, '2018-11-14 06:27:43', '2018-11-14 07:27:50', 'W74552', 67),
            (7, 1, 2, 2, '2018-11-14 07:27:43', '2018-11-14 08:27:50', 'Z64556', 87),
            (8, 1, 3, 2, '2018-11-14 08:27:43', '2018-11-14 09:27:50', 'Z64557', 65),
            (9, 2, 1, 2, '2018-11-14 09:27:43', '2018-11-14 10:27:50', 'Z64553', 34),
            (10, 2, 3, 2, '2018-11-14 10:27:43', '2018-11-14 11:27:50', 'Z64552', 33),
            (11, 3, 1, 2, '2018-11-14 11:27:43', '2018-11-14 12:27:50', 'Z74553', 23),
            (12, 3, 2, 2, '2018-11-14 12:27:43', '2018-11-14 13:27:50', 'Z74552', 122),
            (13, 1, 2, 3, '2018-11-14 13:27:43', '2018-11-14 14:27:50', 'Y64556', 32),
            (14, 1, 3, 3, '2018-11-14 14:27:43', '2018-11-14 15:27:50', 'Y64557', 88),
            (15, 2, 1, 3, '2018-11-14 15:27:43', '2018-11-14 16:27:50', 'Y64553', 55),
            (16, 2, 3, 3, '2018-11-14 16:27:43', '2018-11-14 17:27:50', 'Y64552', 44),
            (17, 3, 1, 3, '2018-11-14 17:27:43', '2018-11-14 18:27:50', 'Y74553', 33),
            (18, 3, 2, 3, '2018-11-14 18:27:43', '2018-11-14 19:27:50', 'Y74552', 22)
            ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM flight WHERE id BETWEEN 1 AND 18');
        $this->addSql('DELETE FROM transporter WHERE id IN (1, 2, 3)');
        $this->addSql('DELETE FROM airport WHERE id IN (1, 2, 3)');
    }
}
