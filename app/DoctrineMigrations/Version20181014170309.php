<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181014170309 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE flight_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE transporter_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE airport_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE flight (
                              id INT NOT NULL, 
                              departure_airport_id INT NOT NULL, 
                              arrival_airport_id INT NOT NULL, 
                              transporter_id INT NOT NULL, 
                              departure_date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
                              arrival_date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
                              duration INT NOT NULL, 
                              flight_number VARCHAR(16) NOT NULL, 
                              PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C257E60EA9F64E43 ON flight (flight_number)');
        $this->addSql('CREATE INDEX IDX_C257E60EF631AB5C ON flight (departure_airport_id)');
        $this->addSql('CREATE INDEX IDX_C257E60E7F43E343 ON flight (arrival_airport_id)');
        $this->addSql('CREATE INDEX IDX_C257E60E4F335C8B ON flight (transporter_id)');
        $this->addSql('CREATE TABLE transporter (
                              id INT NOT NULL, 
                              name VARCHAR(255) NOT NULL, 
                              code VARCHAR(3) NOT NULL, 
                              PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A036E2D477153098 ON transporter (code)');
        $this->addSql('CREATE TABLE airport (
                              id INT NOT NULL, 
                              name VARCHAR(255) NOT NULL, 
                              code VARCHAR(3) NOT NULL, 
                              time_zone VARCHAR(64) NOT NULL, 
                              PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7E91F7C277153098 ON airport (code)');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60EF631AB5C FOREIGN KEY (departure_airport_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E7F43E343 FOREIGN KEY (arrival_airport_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E4F335C8B FOREIGN KEY (transporter_id) REFERENCES transporter (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60E4F335C8B');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60EF631AB5C');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60E7F43E343');
        $this->addSql('DROP SEQUENCE flight_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE transporter_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE airport_id_seq CASCADE');
        $this->addSql('DROP TABLE flight');
        $this->addSql('DROP TABLE transporter');
        $this->addSql('DROP TABLE airport');
    }
}
